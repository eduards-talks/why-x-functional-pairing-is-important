<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## X-Functional Pairing
### The road to better DevOps
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->


----


## DevOps

<img src="/images/devops.jpg" alt="https://unsplash.com/photos/h6xNSDlgciU" height="480px" >

_A set of practices intended to reduce the time between committing a change to a system and the change being placed into production_

Note:
Is a very overloaded term nowadays. Let's define it for the scope of this talk.
Not everyone will agree with this but I believe it suffices for today.

----

## This Talk is about Why Things are Important

<img src="/images/why_important.jpg" alt="https://unsplash.com/photos/3y1zF4hIPCg">

Note:
There are a lot of things that are considered important by a lot of people.
However _if everything is important, nothing is_.

Keep this in mind. It might be important to you that everyone can code in
language X or work with technology Y, but please keep in mind that this is
biased by where you are standing.

If you need to decide what to tackle first, you need to understand what you want
to achieve and perhaps even more importantly why.

----  ----

## Practices are Made by People

<img src="/images/processes_by_people.jpg" alt="https://unsplash.com/photos/zN9GETze9OA">

Note:
At the core of your business are your employees. Anyone that tells you differently does not
understand how humans work.

No matter what you write down if you do not align it with what is actually done
it will be worthless.

Given that your business generates value, your employees are doing something right.
Those are the practices you need to start from.

----

## Practices are about Interaction

<img src="/images/interaction.jpg" alt="https://unsplash.com/photos/h0rXrHzhFXU">

Note:
Remember Conway's law? Roughly: The structure of any system you design is a copy of your communication structure.

So if you want to improve the structure of your systems you will need to improve your
communication structure.

----  ----

## Interaction is about Understanding

<img src="/images/understanding.jpg" alt="https://unsplash.com/photos/6jYoil2GhVk" height="600px">

Note:
If you want to improve the communication structure, you will need to improve the way people
understand each other.

Now what do I mean by that?

Well Understanding is about Context.

----

### Understanding the Pressures Others Face

<!--img src="/images/pressure.jpg" alt="https://unsplash.com/photos/E4944K_4SvI"-->
<img src="/images/line-pressure.jpg" alt="https://unsplash.com/photos/H20OWckc-5E">

Note:
Walking in another ones shoes can greatly increase the context we have concerning
the diverse set of pressures that act on them.

There are always trade offs to consider but while we know this all to well
when it comes to ourselves we tend to simplify or ignore this when it comes to
others.

----

### Understanding the Requirements they see (and you do not)

<img src="/images/requirements.jpg" alt="https://unsplash.com/photos/6oOK1qu84A4" height="680px">

Note:
Sometimes decisions can not be explained by simply following the external forces of the moment.

Sometimes there just are some things we do not see or know about that the other person
is very aware of. Often those things are so clear to them that they do not even
think about the possibility that someone does not know about it.

Understanding their requirements will enable you to understand the values they are
charged to create and protect.

These types of requirements can be uncovered when working side by side for a while.

----

### Understanding the Technologies that Enable Them

<img src="/images/enablement.jpg" alt="https://unsplash.com/photos/rs7M_r4SpuI">

Note:
What we work is important, but I would argue only half as important as how we work.

Gaining insight into what is easy for someone else and what is hard for them gives
multiple benefits.

Not only do you get to understand what is a big ask and what is easy as, you also get
the opportunity to lend a helping hand and share your own tech if you see that it might
fit and help.

A word of warning here. The way people work works for them and is a proven method. They
might be defensive of it at first. Give it time, be kind and understanding.

----

### Understanding what Drives them

<img src="/images/drive.jpg" alt="https://unsplash.com/photos/qnQYO3IY5UM">


Note:
So we talked about the external forces, we talked about the what and the how.
The most important thing however is the why.

Why do they do the things they do?
Are they just in it for the paycheck? That is a perfectly legitimate reason.
Are they fascinated by what they do? That is perfectly fine as well.
Are they here for the interactions with others? Just as perfect.

No matter the reason. Understanding this will help you understanding why they
work the way they do.

----  ----

## Practices are about Uniting Behind a Common Goal

<img src="/images/common-goals-hands.jpg" alt="https://unsplash.com/photos/Dph00R2SwFo" height="550px">

Note:
We all work on the same thing. We do have things in common, we do share goals.

Seeing the other side of the coin, gaining a better concept of the whole goal and
uniting behind that goal to improve our common practices requires intense interaction
on all levels.

----  ----

## At the end of the Day...

<img src="/images/talking.jpg" alt="https://unsplash.com/photos/7tb-EZTB8kk" height="650px">

Note:
...it is about talking to each other and working together on every day tasks.


----

## Listening Takes Time and Effort

<img src="/images/listening.jpg" alt="https://unsplash.com/photos/5OLBUas5epM">

Note:
If you work with someone and want to gain an understanding of how to improve together,
you will not only need to physically hear their words.

You will need to understand, respond and remember what was being said. This is called
active listening and is a skill that can be very beneficial. It is however as all valuable
skills not free.

----

## Understanding Allows for Efficiencies to Emerge

<img src="/images/room-to-grow.jpg" alt="https://unsplash.com/photos/-f8ssjFhD1k" height="550px">

Note:
So now that we have put in all this effort it is fair to ask: 'And what do we gain?'

Well from a purely operational perspective you gain process resilience, the removal of silos,
a better educated work force, better levers for technical and organizational change and a few other things like an increase in efficiency due to lower friction hand-offs, among other things.

On a deeper level, you will gain personal flexibility, a better understanding of what you want
and don't want to do, a few easy ways to make someone elses life easier and some load of your
back as well.
However the maybe most important thing you gain is possibilities. We tend to restrict the other,
but the known, we are usually more lenient there.

----  ----

<img src="/images/questions.jpg" alt="https://unsplash.com/photos/AoqgGAqrLpU">

Note:
Questions?

----  ----

<img src="/images/thanks.jpg" alt="https://unsplash.com/photos/0YbeoQOX89k">

----  ----

### Image Credits

Are contained in the image alts. All images from unsplash.
